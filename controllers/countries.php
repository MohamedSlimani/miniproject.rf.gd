<?php
class Countries extends Controller{

	protected function Add(){
		if (!$_SESSION['user_data']['is_admin']) {
				Messages::setMsg("you don't have Admin access", 'error');
				header('Location: '.ROOT_PATH);
		}
		$viewmodel = new CountrieModel();
		$this->returnView($viewmodel->Add(), true);

	}
}