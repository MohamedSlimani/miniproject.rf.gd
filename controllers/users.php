<?php
class Users extends Controller{
	protected function register(){
		$viewmodel = new UserModel();
		$this->returnView($viewmodel->register(), true);
	}

	protected function login(){
		$viewmodel = new UserModel();
		$this->returnView($viewmodel->login(), true);
	}

	protected function logout(){
		unset($_SESSION['is_logged_in']);
		unset($_SESSION['user_data']);
		$_SESSION['is_logged_out'] = 1;
		$viewmodel = null;
		// Redirect
		header('Location: '.ROOT_PATH);
	}

	protected function profile($user_id){
		$viewmodel = new UserModel();
		$this->returnView($viewmodel->profile($user_id), true);
	}

	protected function settings(){
		$viewmodel = new UserModel();
		$this->returnView($viewmodel->settings(), true);
	}
}