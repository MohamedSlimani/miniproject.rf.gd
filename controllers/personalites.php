<?php
class Personalites extends Controller{
	protected function Index(){
		if(!isset($_SESSION['is_logged_in'])){
			header('Location: '.ROOT_PATH);
		}
		$viewmodel = new PersonalModel();
		$this->returnView($viewmodel->Index(), true);
	}

	protected function add(){
		if (!$_SESSION['user_data']['is_admin']) {
				Messages::setMsg("you don't have Admin access", 'error');
				header('Location: '.ROOT_PATH);
		}elseif(!isset($_SESSION['is_logged_in'])){
			header('Location: '.ROOT_PATH);
		}
		$viewmodel = new PersonalModel();
		$this->returnView($viewmodel->add(), true);
	}

	protected function find($name){
		if(!isset($_SESSION['is_logged_in'])){
			header('Location: '.ROOT_PATH.'Personalites');
		}
		$viewmodel = new PersonalModel();
		$this->returnView($viewmodel->find($name), true);
	}

	protected function id($name){
		if(!isset($_SESSION['is_logged_in'])){
			header('Location: '.ROOT_PATH.'Personalites');
		}
		if (!$name) {
			header('Location: '.ROOT_PATH.'Personalites');
		}
		$viewmodel = new PersonalModel();
		$this->returnView($viewmodel->id($name), true);
	}

	protected function countries($name){
		if(!isset($_SESSION['is_logged_in'])){
			header('Location: '.ROOT_PATH.'Personalites');
		}
		$viewmodel = new PersonalModel();
		$this->returnView($viewmodel->countries($name), true);
	}

	protected function avtivites($name){
		if(!isset($_SESSION['is_logged_in'])){
			header('Location: '.ROOT_PATH.'Personalites');
		}
		$viewmodel = new PersonalModel();
		$this->returnView($viewmodel->avtivites($name), true);
	}

	protected function modify($id){
		if (!$_SESSION['user_data']['is_admin']) {
				Messages::setMsg("you don't have Admin access", 'error');
				header('Location: '.ROOT_PATH);
		}elseif(!$id or !isset($_SESSION['is_logged_in'])){
			header('Location: '.ROOT_PATH);
		}
		$viewmodel = new PersonalModel();
		$this->returnView($viewmodel->modify($id), true);
	}

	protected function delete($id){
		if (!$_SESSION['user_data']['is_admin']) {
				Messages::setMsg("you don't have Admin access", 'error');
				header('Location: '.ROOT_PATH);
		}elseif(!$id or !isset($_SESSION['is_logged_in'])){
			header('Location: '.ROOT_PATH);
		}
		$viewmodel = new PersonalModel();
		$this->returnView($viewmodel->delete($id), true);
	}

}