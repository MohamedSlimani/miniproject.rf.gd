<?php
class Statistics extends Controller{
	protected function index(){
		$viewmodel = new StatisticModel();
		$this->returnView($viewmodel->index(), true);
	}

	protected function activiteyear(){
		$viewmodel = new StatisticModel();
		$this->returnView($viewmodel->activiteyear(), true);	
	}

	protected function natdate(){
		$viewmodel = new StatisticModel();
		$this->returnView($viewmodel->natdate(), true);	
	}

	protected function natactivite(){
		$viewmodel = new StatisticModel();
		$this->returnView($viewmodel->natactivite(), true);	
	}

}