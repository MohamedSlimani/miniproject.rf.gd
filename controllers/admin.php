<?php
class Admin extends Controller{

	protected function AddUser(){
		if (!$_SESSION['user_data']['is_admin']) {
				Messages::setMsg("you don't have Admin access", 'error');
				header('Location: '.ROOT_PATH);
		}
		$viewmodel = new AdminModel();
		$this->returnView($viewmodel->AddUser(), true);

	}

	protected function ModifyUser($username){
		if (!$_SESSION['user_data']['is_admin']) {
				Messages::setMsg("you don't have Admin access", 'error');
				header('Location: '.ROOT_PATH);
		}
		$viewmodel = new AdminModel();
		$this->returnView($viewmodel->ModifyUser($username), true);
	}

	protected function ChoseUser(){
		if (!$_SESSION['user_data']['is_admin']) {
				Messages::setMsg("you don't have Admin access", 'error');
				header('Location: '.ROOT_PATH);
		}
		$viewmodel = new AdminModel();
		$this->returnView($viewmodel->ChoseUser(), true);
	}

	protected function DeletUser($username){
		if (!$_SESSION['user_data']['is_admin']) {
				Messages::setMsg("you don't have Admin access", 'error');
				header('Location: '.ROOT_PATH);
		}
		$viewmodel = new AdminModel();
		$this->returnView($viewmodel->DeletUser($username), true);
	}
}