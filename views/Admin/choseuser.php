<script src="<?php echo ROOT_PATH; ?>assets/js/corp.image.js"></script>

<h1 class="page-header">Chose user</h1>
<?php foreach ($viewmodel as $key => $value) : ?>
<div class="panel panel-default">
    <div class="panel-heading">
    <h3 class="panel-title">user informations</h3>
    </div>
    <div class="panel-body">
        <div class="col-sm-3" style="overflow: hidden;">
            <div class="corpdiv preview" >
                <img class="img-thumbnail" id="result" onclick="openFileOption();return;" src="<?php echo ROOT_PATH.$value['img']; ?>" id="profile" alt="profile pic" style="cursor:pointer">
                <input id="file1" type="file" onchange="loadFile(event)" style="display:none">
            </div> 
        </div>
        <div class="col-sm-9">
            <h1><?php echo $value['first_name'].' '.$value['last_name']; ?></h1>
            <h3><?php echo $value['email']; ?></h3>
            <h3><a href="<?php echo ROOT_PATH.'admin/modifyuser/'.$value['username']; ?>">@<?php echo $value['username']; ?></a></h3>
        </div>

    </div>
    <div class="panel-footer ">
    <button class="btn btn-primary" style="visibility: hidden;">Primary</button>
        <div class="pull-right">
                <a href="<?php echo ROOT_PATH.'admin/modifyuser/'.$value['username']; ?>" class="btn btn-primary">Modifer</a>
                <a href="<?php echo ROOT_PATH.'admin/deletuser/'.$value['username']; ?>" class="btn btn-danger">Supprimer</a>
        </div>
    </div>
</div>
<!-- /.panel -->
<?php endforeach; ?>
