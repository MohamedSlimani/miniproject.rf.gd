<script src="<?php echo ROOT_PATH; ?>assets/js/corp.image.js"></script>
<h1 class="page-header">Chose user</h1>
<div class="panel panel-default">
    <div class="panel-heading">
    <h3 class="panel-title">user informations</h3>
    </div>
    <div class="panel-body">
        <div class="col-sm-3" style="overflow: hidden;">
            <div class="corpdiv preview" >
                <img class="img-thumbnail" id="result" onclick="openFileOption();return;" src="<?php echo ROOT_PATH.$viewmodel['img']; ?>" id="profile" alt="profile pic" style="cursor:pointer">
                <input id="file1" type="file" onchange="loadFile(event)" style="display:none">
            </div> 
        </div>
        <div class="col-sm-9">
            <h1><?php echo $viewmodel['first_name'].' '.$viewmodel['last_name']; ?></h1>
            <h3><?php echo $viewmodel['email']; ?></h3>
            <h3><a href="<?php echo ROOT_PATH.'admin/modifyuser/'.$value['username']; ?>">@<?php echo $viewmodel['username']; ?></a></h3>
        </div>

    </div>
    <div class="panel-footer ">
    <button class="btn btn-primary" style="visibility: hidden;">Primary</button>
        <div class="pull-right">
                <a href="<?php echo ROOT_PATH.'admin/modifyuser/'.$value['username']; ?>" class="btn btn-primary">Annuler</a>
                <input type="submit" name="submit" class="btn btn-danger" form="conforme" value="Confirmer la supprition">
        </div>
    </div>
</div>
<form id="conforme" method="post" action="<?php $_SERVER['PHP_SELF']; ?>" enctype="multipart/form-data">
    
</form>
<!-- /.panel -->
