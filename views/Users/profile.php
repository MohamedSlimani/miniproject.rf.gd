<?php if ($viewmodel):?>
<script src="<?php echo ROOT_PATH; ?>assets/js/profile.js"></script>
<div class="user-profile col-sm-12" >
    <img align="left" class="user-image-lg" src="<?php echo ROOT_PATH;?>/uploads/images/5.jpg" alt="cover image"/>
    	<img align="left"
    	<?php if ($viewmodel['modifie']) {
    		echo'data-toggle="tooltip" data-placement="bottom" title="Click to change the image"';
    	} ?>
    	class="user-image-profile thumbnail" src="<?php echo ROOT_PATH.$viewmodel['user_data']['img']; ?>" alt="Profile image"/>
    <div class="user-profile-text">
        <h1><?php echo($viewmodel['user_data']['first_name']." ".$viewmodel['user_data']['last_name']) ?></h1>
        <p>@<?php echo($viewmodel['user_data']['username']) ?></p>
    </div>
<div class="panel panel-default">
    <!-- /.panel-heading -->
    <div class="panel-body">
        <div>
			<?php if($_SESSION['user_data']['is_admin']) : ?>
			<a class="btn btn-success btn-share btn-block" href="<?php echo ROOT_PATH; ?>Personalites/add" style="margin-bottom: 20px">add Personalite</a>
			<?php else: ?>
                <a class="btn btn-primary btn-share btn-block" href="<?php echo ROOT_PATH; ?>users/settings" style="margin-bottom: 20px"><i class="fa fa-gear fa-fw"></i> modifer vos parrametres</a>
            <?php endif; ?>
		</div>
   	</div>
    <!-- /.panel-body -->
</div>
</div>
<?php else: ?>
	<?php if(!isset($_SESSION['is_logged_in'])) : ?>
        <a class="btn btn-primary text-center" href="<?php echo ROOT_PATH;?>">Home</a>
      <?php else : ?>
        <a class="btn btn-primary text-center" href="<?php echo ROOT_PATH;?>Personalites">Home</a>
      <?php endif; ?>
<?php endif; ?>
