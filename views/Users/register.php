<div style="height: 70px;"></div>
<div class="container"><div class="row">
<script src="<?php echo ROOT_PATH; ?>assets/js/corp.image.js"></script>
<style type="text/css">
  img {
    max-width: 100%; /* This rule is very important*/
  }
</style>

<!-- Bootstrap Date-Picker Plugin -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
<script>
    $(document).ready(function(){
      var date_input=$('input[name="birthday"]');
      var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
      var options={
        format: 'yyyy-mm-dd',
        container: container,
        todayHighlight: true,
        autoclose: true,
      };
      date_input.datepicker(options);
    })
</script>



<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Inscription</h3>
  </div>
  <div class="panel-body">
    <div class="col-sm-3" style="overflow: hidden;">
        <div class="corpdiv preview" >
        <img class="img-thumbnail" id="result" onclick="openFileOption();return;" src="<?php echo ROOT_PATH; ?>uploads/images/ProfilePicture.png" id="profile" alt="profile pic" style="cursor:pointer">
        <input id="file1" type="file" onchange="loadFile(event)" style="display:none">
        </div>
        
        
    </div>
    <form method="post" id="registration" action="<?php $_SERVER['PHP_SELF']; ?>" enctype="multipart/form-data" class="form-horizontal col-sm-9"> 
        <div class="form-group">
            <div class="control-label col-sm-3"></div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="first_name">Nom:</label>
            <div class="col-sm-9">
                <input type="text" name="first_name"  class="form-control" placeholder="Votre nom" required>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="last_name">Prénom:</label>
            <div class="col-sm-9">
                <input type="text" name="last_name"  class="form-control" placeholder="Votre prénom" required>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="birthday">Date de nissance:</label>
            <div class="col-sm-9">
                <input class="form-control" id="date" name="birthday" placeholder="AAAA-MM-JJ" type="text" required>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="email">Email:</label>
            <div class="col-sm-9">
                <input type="email" name="email" class="form-control" id="email" placeholder="exemple@exemple.com" required>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="pwd">Mot de pass:</label>
            <div class="col-sm-9">
                <input type="password" name="password" class="form-control" id="pwd" onfocusout="pass()" placeholder="Mot de pass" required>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="pwdcmf">Confirmier:</label>
            <div class="col-sm-9">
                <input type="password" name="password" class="form-control" id="pwdcmf" onfocusout="confpass()" placeholder="Confirmier mot de pass" required>
            </div>
        </div>
        <div class="form-group">
                <label class="control-label col-sm-3" for="sex">sex:</label>
                <div class="col-sm-9">
                  <select class="form-control" id="sex" name="sex">
                    <option value="M">Homme</option>
                    <option value="F">Famme</option>
                  </select>
                </div>
                </div>
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-9">
                <input class="btn btn-primary" name="submit" type="submit" value="Submit" />
            </div>
        </div>
        <input type="hidden" id="ProfilePicture" name="ProfilePicture" form="registration">
    </form>
  </div>
</div>
<div class="modal fade" id="modal" aria-labelledby="modalLabel" role="dialog" tabindex="-1">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalLabel">Crop the image</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <div>
          <img id="image" src="<?php echo ROOT_PATH; ?>uploads/images/ProfilePicture.png" width="304" style="display: block; margin: auto;" alt="Picture">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
      </div>
    </div>
  </div>
</div>
</div>
</div>
