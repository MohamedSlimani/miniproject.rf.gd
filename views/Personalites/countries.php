<?php if (isset($viewmodel['nbrs'])) :?>
<div class="col-lg-12">
    <h1 class="page-header">Pays</h1>
</div>
<!-- /.col-lg-12 -->
<div class="row">
    <?php foreach ($viewmodel['pays'] as $key => $pays):?>
    <div class="col-lg-4 col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
            <strong><?php echo $pays['label'] ?></strong>
            </div>
            <div class="panel-body">
                <div class="corpdiv preview">
                <img class="img-thumbnail" src="<?php echo ROOT_PATH.'uploads/Countries/'.$pays['label'].'.svg'; ?>" alt="profile pic" >
                </div>
            </div>
            <a href="<?php echo ROOT_PATH.'/Personalites/countries/'.$pays['id']; ?>">
                <div class="panel-footer">
                    <span class="pull-left"><?php echo $viewmodel['nbrs'][$pays['id']]; ?> Personalités</span>
                    <span class="pull-right">Voir plus <i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
<?php endforeach; ?>
</div>
<?php else : ?>
<div class="col-lg-12">
    <h1 class="page-header">Resultat de recherch par pays : 
    <i> <?php echo $viewmodel['personal'][0]['pays']; ?>
    </i>
    </h1>
</div>
<!-- /.col-lg-12 -->

<div class="row">
<div class="col-lg-12">
    <?php foreach ($viewmodel['personal'] as $key => $value) : ?>
    <div class="panel panel-default">
        <div class="panel-heading">
        <h3 class="panel-title">informations de personamité</h3>
        </div>
        <div class="panel-body">
            <div class="col-sm-3" style="overflow: hidden;">
                <div class="corpdiv preview" >
                    <img class="img-thumbnail" src="<?php echo ROOT_PATH.$value['image']; ?>" id="profile" alt="profile pic">
                </div> 
            </div>
            <div class="col-sm-9">
                <h1><?php echo $value['first_name'].' '.$value['last_name']; ?></h1>
                <h3><strong>email: </strong><?php echo $value['email']; ?></h3>
                <h3><strong>activite: </strong><?php echo $value['activite']; ?></h3>
                <h3><strong>pays: </strong><?php echo $value['pays']; ?></h3>
            </div>

        </div>
        <div class="panel-footer ">
        <button class="btn btn-success" style="visibility: hidden;">Primary</button>
            <div class="pull-right">
                    <a href="<?php echo ROOT_PATH.'Personalites/id/'.$value['N_ordre']; ?>" class="btn btn-success">Voir</a>
            </div>
        </div>
    </div>
    <!-- /.panel -->
    <?php endforeach; ?>

    <!-- /.panel -->
</div>
<!-- /.col-lg-12 -->
</div>
<?php endif; ?>