<div class="col-lg-12">
    <h1 class="page-header">Ajouté récemment</h1>
</div>
<!-- /.col-lg-12 -->
<!-- /.row -->
<div class="row">
<div class="col-lg-4 col-md-6">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <div class="row">
                <div class="col-xs-3">
                    <i class="fa fa-id-card-o fa-5x"></i>
                </div>
                <div class="col-xs-9 text-right">
                    <div class="huge"><?php echo $viewmodel['nbrs']['personal']; ?></div>
                    <div>Personalité!</div>
                </div>
            </div>
        </div>
        <a href="<?php echo ROOT_PATH.'Personalites/find'; ?>">
            <div class="panel-footer">
                <span class="pull-left">View Details</span>
                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                <div class="clearfix"></div>
            </div>
        </a>
    </div>
</div>
<div class="col-lg-4 col-md-6">
    <div class="panel panel-green">
        <div class="panel-heading">
            <div class="row">
                <div class="col-xs-3">
                    <i class="fa fa-flag fa-5x"></i>
                </div>
                <div class="col-xs-9 text-right">
                    <div class="huge"><?php echo $viewmodel['nbrs']['pays']; ?></div>
                    <div>pays!</div>
                </div>
            </div>
        </div>
        <a href="<?php echo ROOT_PATH.'Personalites/countries'; ?>">
            <div class="panel-footer">
                <span class="pull-left">View Details</span>
                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                <div class="clearfix"></div>
            </div>
        </a>
    </div>
</div>
<div class="col-lg-4 col-md-6">
    <div class="panel panel-yellow">
        <div class="panel-heading">
            <div class="row">
                <div class="col-xs-3">
                    <i class="fa fa-suitcase fa-5x"></i>
                </div>
                <div class="col-xs-9 text-right">
                    <div class="huge"><?php echo $viewmodel['nbrs']['activite']; ?></div>
                    <div>activites!</div>
                </div>
            </div>
        </div>
        <a href="<?php echo ROOT_PATH.'Personalites/avtivites'; ?>">
            <div class="panel-footer">
                <span class="pull-left">View Details</span>
                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                <div class="clearfix"></div>
            </div>
        </a>
    </div>
</div>
</div>
<!-- /.row -->
<div class="row">
<div class="col-lg-12">
    <?php foreach ($viewmodel['personal'] as $key => $value) : ?>
    <div class="panel panel-default">
        <div class="panel-heading">
        <h3 class="panel-title">informations de personamité</h3>
        </div>
        <div class="panel-body">
            <div class="col-sm-3" style="overflow: hidden;">
                <div class="corpdiv preview" >
                    <img class="img-thumbnail" src="<?php echo ROOT_PATH.$value['image']; ?>" id="profile" alt="profile pic">
                </div> 
            </div>
            <div class="col-sm-9">
                <h1><?php echo $value['first_name'].' '.$value['last_name']; ?></h1>
                <h3><strong>email: </strong><?php echo $value['email']; ?></h3>
                <h3><strong>activite: </strong><?php echo $value['activite']; ?></h3>
                <h3><strong>pays: </strong><?php echo $value['pays']; ?></h3>
            </div>

        </div>
        <div class="panel-footer ">
        <button class="btn btn-success" style="visibility: hidden;">Primary</button>
            <div class="pull-right">
                    <a href="<?php echo ROOT_PATH.'Personalites/id/'.$value['N_ordre']; ?>" class="btn btn-success">Voir</a>
            </div>
        </div>
    </div>
    <!-- /.panel -->
    <?php endforeach; ?>
    <!-- /.panel -->
</div>
<!-- /.col-lg-12 -->
</div>