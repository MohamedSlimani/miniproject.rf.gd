<div class="col-lg-12">
    <h1 class="page-header">Confirmier la supprission</h1>
</div>
<!-- /.col-lg-12 -->
<form id="delete" method="post" action="<?php $_SERVER['PHP_SELF']; ?>"></form>
<div class="row">
<div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading">
        <h3 class="panel-title">informations de personamité</h3>
        </div>
        <div class="panel-body">
            <div class="col-sm-3" style="overflow: hidden;">
                <div class="corpdiv preview" >
                    <img class="img-thumbnail" src="<?php echo ROOT_PATH.$viewmodel['personal']['image']; ?>" id="profile" alt="profile pic">
                </div> 
            </div>
            <div class="col-sm-9">
                <h1><?php echo $viewmodel['personal']['first_name'].' '.$viewmodel['personal']['last_name']; ?></h1>
                <h3><strong>email: </strong><?php echo $viewmodel['personal']['email']; ?></h3>
                <h3><strong>activite: </strong><?php echo $viewmodel['activite']['label']; ?></h3>
                <h3><strong>pays: </strong><?php echo $viewmodel['pays']['label']; ?></h3>
            </div>

        </div>
        <div class="panel-footer ">
        <button class="btn btn-success" style="visibility: hidden;">Primary</button>
            <div class="pull-right">
                    <a href="<?php echo ROOT_PATH.'Personalites/id/'.$viewmodel['personal']['N_ordre']; ?>" class="btn btn-success">Annuler</a>
                    <input type="submit" name="submit" id="submit" class="btn btn-danger" form="delete" value="Supprimier">
            </div>
        </div>
    </div>
    <!-- /.panel -->
</div>
<!-- /.col-lg-12 -->
</div>