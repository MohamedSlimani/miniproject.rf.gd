<div class="col-lg-12">
    <h1 class="page-header"><?php echo $viewmodel['first_name'].' '.$viewmodel['last_name']; ?></h1>
</div>
<!-- /.col-lg-12 -->
<div class="row">
    <div class="col-lg-3 col-lg-push-9">
        <div class="panel panel-default">
            <div class="panel-heading">
                <?php echo $viewmodel['first_name'].' '.$viewmodel['last_name']; ?>
            </div>
            <div class="panel-body">
                <div class="corpdiv preview" style="margin: auto;">
                <img class="img-thumbnail" id="result" src="<?php echo ROOT_PATH.$viewmodel['image']; ?>" id="profile" alt="profile pic" >
                </div>
                 <button class="btn btn-default btn-block" data-toggle="collapse" data-target="#info">Plus de Details</button>
                <div id="info" class="collapse">
                    <strong>Nationalité :</strong><?php echo $viewmodel['pays_nat']; ?><br>
                    <strong>Naissance :</strong><?php echo $viewmodel['dat_nais'].' '.$viewmodel['lieu_nais'].' ('.$viewmodel['pays_nais'].')'; ?><br>
                    <strong>Adresse :</strong><?php echo $viewmodel['adresse'].' '.$viewmodel['ville'].' ('.$viewmodel['paye'].')'; ?><br>
                    <strong>Email :</strong></strong><?php echo $viewmodel['email']; ?><br>
                    <strong>Profession  :</strong></strong><?php echo $viewmodel['profession'].$viewmodel['address_prof']; ?><br>
                    <strong>Telephone :</strong></strong><?php echo $viewmodel['tel']; ?><br>
                </div>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-4 -->
    <div class="col-lg-9  col-lg-pull-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                Biographie :
                <?php if ($_SESSION['user_data']['is_admin']): ?>
                <div class="pull-right">
                    <div class="btn-group">
                        <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                            Actions
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu pull-right" role="menu">
                            <li>
                            <?php echo '<a href="'.ROOT_PATH.'personalites/modify/'.$viewmodel['N_ordre'].'">Modify</a>';?>
                            </li>
                            <li>
                            <?php echo '<a href="'.ROOT_PATH.'personalites/delete/'.$viewmodel['N_ordre'].'">Supprimer</a>';?>
                            </li>
                        </ul>
                    </div>
                </div>
                <?php endif; ?>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <?php echo htmlspecialchars_decode($viewmodel['historique']); ?>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
        
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-8 -->
    
</div>