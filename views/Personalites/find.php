<div class="col-lg-12">
    <h1 class="page-header">Resultat de recherch sur :
    <i>
    <?php if($viewmodel['name']){
            echo $viewmodel['name'];
        }else{
            echo "tout les Personalités";
            }
    ?>
    </i>
    </h1>
</div>
<!-- /.col-lg-12 -->

<div class="row">
<div class="col-lg-12">
    <?php foreach ($viewmodel['personal'] as $key => $value) : ?>
    <div class="panel panel-default">
        <div class="panel-heading">
        <h3 class="panel-title">informations de personamité</h3>
        </div>
        <div class="panel-body">
            <div class="col-sm-3" style="overflow: hidden;">
                <div class="corpdiv preview" >
                    <img class="img-thumbnail" src="<?php echo ROOT_PATH.$value['image']; ?>" id="profile" alt="profile pic">
                </div> 
            </div>
            <div class="col-sm-9">
                <h1><?php echo $value['first_name'].' '.$value['last_name']; ?></h1>
                <h3><strong>email: </strong><?php echo $value['email']; ?></h3>
                <h3><strong>activite: </strong><?php echo $value['activite']; ?></h3>
                <h3><strong>pays: </strong><?php echo $value['pays']; ?></h3>
            </div>

        </div>
        <div class="panel-footer ">
        <button class="btn btn-success" style="visibility: hidden;">Primary</button>
            <div class="pull-right">
                    <a href="<?php echo ROOT_PATH.'Personalites/id/'.$value['N_ordre']; ?>" class="btn btn-success">Voir</a>
            </div>
        </div>
    </div>
    <!-- /.panel -->
    <?php endforeach; ?>

    <!-- /.panel -->
</div>
<!-- /.col-lg-12 -->
</div>