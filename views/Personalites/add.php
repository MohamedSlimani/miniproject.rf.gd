<script src="<?php echo ROOT_PATH; ?>assets/js/corp.image.js"></script>
<script src="<?php echo ROOT_PATH; ?>assets/ckeditor/ckeditor.js"></script>
<script src="<?php echo ROOT_PATH; ?>assets/ckeditor/js/sample.js"></script>
<style type="text/css">
  img {
    max-width: 100%; /* This rule is very important*/
  }
</style>

<!-- Bootstrap Date-Picker Plugin -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
<script>
    $(document).ready(function(){
      var date_input=$('input[name="birthday"]');
      var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
      var options={
        format: 'yyyy-mm-dd',
        container: container,
        todayHighlight: true,
        autoclose: true,
      };
      date_input.datepicker(options);
    })
</script>


<h1 class="page-header">Ajouter personalité</h1>
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Les informatons d'une personalité</h3>
  </div>
  <div class="panel-body">
    <div class="col-sm-3" style="overflow: hidden;">
        <div class="corpdiv preview" >
        <img class="img-thumbnail" id="result" onclick="openFileOption();return;" src="<?php echo ROOT_PATH; ?>uploads/images/ProfilePicture.png" id="profile" alt="profile pic" style="cursor:pointer">
        <input id="file1" type="file" onchange="loadFile(event)" style="display:none">
        </div>
        <div class="corpdiv preview" >
        <img class="img-thumbnail" src="<?php echo ROOT_PATH; ?>uploads/Countries/<?php echo $viewmodel['pays']['0']['label']; ?>.svg" id="flag" alt="flag pic">
        </div>   
    </div>
    <form method="post" id="registration" action="<?php $_SERVER['PHP_SELF']; ?>" enctype="multipart/form-data" class="form-horizontal col-sm-9"> 
        <div class="form-group">
            <div class="control-label col-sm-3"></div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="Nationalite">pays Nationalité:</label>
            <div class="col-sm-9">
                <select class="form-control" onchange="flg()" id="Nationalite" name="Nationalite">
                <?php foreach ($viewmodel['pays'] as $value): ?>
                    <option value="<?php echo $value['id']; ?>" ><?php echo $value['label']; ?></option>
                <?php endforeach; ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="Adresse">Adresse:</label>
            <div class="col-sm-9">
                <input type="text" name="Adresse"  class="form-control" placeholder="Adresse" >
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="Ville">Ville:</label>
            <div class="col-sm-9">
                <input type="text" name="Ville"  class="form-control" placeholder="Ville" >
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="pays">pays:</label>
            <div class="col-sm-9">
                <select class="form-control" id="pays" name="pays">
                <?php foreach ($viewmodel['pays'] as $value): ?>
                    <option value="<?php echo $value['id']; ?>" ><?php echo $value['label']; ?></option>
                <?php endforeach; ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="email">Email:</label>
            <div class="col-sm-9">
                <input type="email" name="email" class="form-control" id="email" placeholder="exemple@exemple.com" >
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="first_name">Nom:</label>
            <div class="col-sm-9">
                <input type="text" name="first_name"  class="form-control" placeholder="Votre nom" >
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="last_name">Prénom:</label>
            <div class="col-sm-9">
                <input type="text" name="last_name"  class="form-control" placeholder="Votre prénom" >
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="birthday">Date de nissance:</label>
            <div class="col-sm-9">
                <input class="form-control" id="date" name="birthday" placeholder="AAAA-MM-JJ" type="text"/>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="lieu_de_nissance">lieu de nissance:</label>
            <div class="col-sm-9">
                <input type="text" name="lieu_de_nissance"  class="form-control" placeholder="lieu_de_nissance" >
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="pays_de_nissance">pays de nissance:</label>
            <div class="col-sm-9">
                <select class="form-control" id="pays_de_nissance" name="pays_de_nissance">
                <?php foreach ($viewmodel['pays'] as $value): ?>
                    <option value="<?php echo $value['id']; ?>" ><?php echo $value['label']; ?></option>
                <?php endforeach; ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="Profession">Profession:</label>
            <div class="col-sm-9">
                <input type="text" name="Profession"  class="form-control" placeholder="Profession" >
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="Adresse_Profession">Adresse Profession:</label>
            <div class="col-sm-9">
                <input type="text" name="Adresse_Profession"  class="form-control" placeholder="Adresse_Profession" >
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="Telephone">Telephone:</label>
            <div class="col-sm-9">
                <input type="text" name="Telephone"  class="form-control" placeholder="Telephone" >
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="Annee_debut">Annee debut d'ativité:</label>
            <div class="col-sm-9">
                <input type="text" name="Annee_debut"  class="form-control" placeholder="2017" >
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="avtivite">Type d'avtivite:</label>
            <div class="col-sm-9">
                <select class="form-control" id="avtivite" name="avtivite">
                <?php foreach ($viewmodel['avtivite'] as $value): ?>
                    <option value="<?php echo $value['id']; ?>" ><?php echo $value['label']; ?></option>
                <?php endforeach; ?>
                </select>
            </div>
        </div>
        <input type="hidden" id="ProfilePicture" name="ProfilePicture" form="registration">
    </form>
    <div class="col-sm-12">
        
    </div>
    <div class="col-sm-12">
        <textarea cols="80" id="historique" name="historique" form="registration" rows="10"></textarea>
        <br>
        <input class="btn btn-primary" name="submit" type="submit" form="registration" value="Ajouté" />
    </div>
    

  </div>
</div>
<div class="modal fade" id="modal" aria-labelledby="modalLabel" role="dialog" tabindex="-1">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalLabel">Crop the image</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <div>
          <img id="image" src="<?php echo ROOT_PATH; ?>uploads/images/ProfilePicture.png" width="304" style="display: block; margin: auto;" alt="Picture">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
      </div>
    </div>
  </div>
</div>
<script>
initSample();

function flg() {
    var optionindex = document.getElementById('Nationalite').selectedIndex;
    var options = document.getElementById('Nationalite').options;
    console.log('/uploads/Countries/'+options[optionindex].innerHTML+'.svg');
    document.getElementById('flag').src = '/uploads/Countries/'+options[optionindex].innerHTML+'.svg';
}

</script>
<script>
    CKEDITOR.replace( 'historique' );
</script>