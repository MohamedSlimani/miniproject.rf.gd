<!-- Navigation -->
<nav class="sid-navbar" role="navigation" style="margin-bottom: 0;position: fixed;">
    
    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
                <li>
                    <a href="<?php echo ROOT_PATH; ?>Personalites"><i class="fa fa-dashboard fa-fw"></i> ajoute récemment</a>
                </li>
                <li>
                    <a href="<?php echo ROOT_PATH; ?>Personalites/countries"><i class="fa fa-flag fa-fw"></i> Payes</a>
                </li>
                <li>
                    <a href="<?php echo ROOT_PATH; ?>Personalites/avtivites"><i class="fa fa-suitcase fa-fw"></i> Avtivités</a>
                </li>
                <li>
                    <a href="<?php echo ROOT_PATH; ?>Statistics"><i class="fa fa-bar-chart-o fa-fw"></i> Statistiques</a>
                </li>
                <?php if ($_SESSION['user_data']['is_admin']): ?>
                <li>
                    <a href="<?php echo ROOT_PATH; ?>Personalites/add"><i class="fa fa fa-id-card-o fa-fw"></i> Ajoute une personneleté</a>
                </li>                
                <li>
                    <a href="<?php echo ROOT_PATH; ?>Countries/add"><i class="fa fa-flag-o fa-fw"></i> Ajoute pays</a>
                </li>
                <li>
                    <a href="<?php echo ROOT_PATH; ?>Activitys/add"><i class="fa fa-briefcase fa-fw"></i> Ajoute activité</a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-group fa-fw"></i> Gestion des utilisateurs<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="<?php echo ROOT_PATH; ?>admin/adduser">Ajoute un utilisateur</a>
                        </li>
                        <li>
                            <a href="<?php echo ROOT_PATH; ?>admin/choseuser">Edite un utilisateur</a>
                        </li>
                    </ul>
                    <!-- /.nav-second-level -->
                </li>
                <?php endif; ?>
                
                <li>
                    <a href="<?php echo ROOT_PATH; ?>users/settings"><i class="fa fa-gear fa-fw"></i> Setting</a>
                </li>
            </ul>
        </div>
        <!-- /.sidebar-collapse -->
    </div>
    <!-- /.navbar-static-side -->
</nav>