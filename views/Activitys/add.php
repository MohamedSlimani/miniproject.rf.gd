<script src="<?php echo ROOT_PATH; ?>assets/js/corp.activite.js"></script>
<h1 class="page-header">Ajouter Avtivité</h1>
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Les informatons de activité</h3>
  </div>
  <div class="panel-body">
    <div class="col-sm-3" style="overflow: hidden;">
        <div class="corpdiv preview" >
        <img class="img-thumbnail" id="result" onclick="openFileOption();return;" src="<?php echo ROOT_PATH; ?>uploads/images/flag.svg" id="profile" alt="profile pic" style="cursor:pointer">
        <input id="file1" type="file" onchange="loadFile(event)" style="display:none">
        </div>
           
    </div>
    <form method="post" id="registration" action="<?php $_SERVER['PHP_SELF']; ?>" enctype="multipart/form-data" class="form-horizontal col-sm-9"> 
        <div class="form-group">
            <div class="control-label col-sm-3"></div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="avtivity">Nom:</label>
            <div class="col-sm-9">
                <input type="text" name="avtivity"  class="form-control" placeholder="nom" required>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="descreption">description:</label>
            <div class="col-sm-9">
                <textarea name="descreption"  class="form-control" placeholder="descreption" ></textarea>
            </div>
        </div>
        <input type="hidden" id="ProfilePicture" name="ProfilePicture" form="registration" required>
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-9">
                <input class="btn btn-primary" name="submit" type="submit" value="Submit" />
            </div>
        </div>
    </form>
  </div>
</div>

<div class="modal fade" id="modal" aria-labelledby="modalLabel" role="dialog" tabindex="-1">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalLabel">Crop the image</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <div>
          <img id="image" src="<?php echo ROOT_PATH; ?>uploads/images/ProfilePicture.png" width="304" style="display: block; margin: auto;" alt="Picture">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
      </div>
    </div>
  </div>
</div>
