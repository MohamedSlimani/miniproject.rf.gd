<?php print_r($viewmodel);
function random_color_part() {
    return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
}

function random_color() {
    return random_color_part() . random_color_part() . random_color_part();
}
?>
<script src="<?php echo ROOT_PATH; ?>assets/vendor/charts.js/Chart.min.js"> ></script>
<h1 class="page-header">Statistics Par Nationalité et Année de debut d'activité</h1>
<?php if($viewmodel['activite']): ?>
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Statistique de la nationalite : <?php echo $viewmodel['nat']['label']?></h3>
  </div>
  <div class="panel-body">
  <div class="col-md-6">
    <canvas id="myChart" width="400" height="400"></canvas>
  </div>
  </div>
</div>
<?php else: ?>
    <p>aucun resultat!</p>
<?php endif; ?>
<script>
  var data = {
    labels: [
    <?php
      $len = count($viewmodel['activite']);
      foreach ($viewmodel['activite'] as $value){
        echo '"'.$value['act'].'"';
        if ($len != 0) echo ",";
        $len--;
      };
    ?>
    ],
    datasets: [
        {
            data: [
              <?php
              $len = count($viewmodel['activite']);
              foreach ($viewmodel['activite'] as $value){
                echo $value['nbr'];
                if ($len != 0) echo ",";
                $len--;
              };
              ?>
            ],
            backgroundColor: [
              <?php
              $color;
              $len = count($viewmodel['activite']);
              foreach ($viewmodel['activite'] as $value){
                $tmp = random_color();
                $color[] = $tmp;
                echo '"#'.$tmp.'"';
                if ($len != 0) echo ",";
                $len--;
              };
              ?>
            ],
            hoverBackgroundColor: [
                <?php
              $len = count($color);
              foreach ($color as $value){
                echo '"#'.$value.'"';
                if ($len != 0) echo ",";
                $len--;
              };
              ?>
            ]
        }]
  };
  var ctx = $("#myChart");
  var myDoughnutChart = new Chart(ctx, {
    type: 'pie',
    data: data,
    options: {
        animation:{
            animateScale:true
        }
    }
  });

</script>