<?php print_r($viewmodel) ?>

<h1 class="page-header">Statistics</h1>
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Par année de debut d'activité</h3>
  </div>
  <div class="panel-body">
    <form method="post" action="<?php echo ROOT_PATH; ?>Statistics/activiteyear" enctype="multipart/form-data" class="form-horizontal col-sm-12"> 
        <div class="form-group">
            <div class="control-label col-sm-3"></div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="date_debut">Date de debut:</label>
            <div class="col-sm-10">
                <input type="number" name="date_debut" class="form-control" min="0" max="<?php echo date("Y"); ?>" value="<?php echo date("Y"); ?>" required>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <input class="btn btn-primary" name="submityear" type="submit" value="Afficher" />
            </div>
        </div>
    </form>
  </div>
</div>


<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Par nationalité</h3>
    </div>
    <!-- /.panel-heading -->
    <div class="panel-body">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs">
            <li class="active"><a href="#nattyp" data-toggle="tab">par pays nationalité et type activité</a>
            </li>
            <li><a href="#natdat" data-toggle="tab">par pays nationalité et année début activité</a>
            </li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <div class="tab-pane fade in active" id="nattyp">
                <form method="post" action="<?php echo ROOT_PATH; ?>Statistics/natactivite" enctype="multipart/form-data" class="form-horizontal col-sm-12"> 
                    <div class="form-group">
                        <div class="control-label col-sm-3"></div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="nat">Nationalité:</label>
                        <div class="col-sm-10">
                            <select class="form-control" id="nat" name="nat" required>
                            <?php foreach ($viewmodel['nat'] as $key => $value): ?>
                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                            <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <input class="btn btn-primary" name="submit" type="submit" value="Afficher" />
                        </div>
                    </div>
                </form>
            </div>
            <div class="tab-pane fade" id="natdat">
                <form method="post" action="<?php echo ROOT_PATH; ?>Statistics/natdate" enctype="multipart/form-data" class="form-horizontal col-sm-12"> 
                    <div class="form-group">
                        <div class="control-label col-sm-3"></div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="nat">Nationalité:</label>
                        <div class="col-sm-10">
                            <select class="form-control" id="nat" name="nat" required>
                            <?php foreach ($viewmodel['nat'] as $key => $value): ?>
                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                            <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <input class="btn btn-primary" name="submit" type="submit" value="Afficher" />
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /.panel-body -->
</div>
        <!-- /.panel -->
