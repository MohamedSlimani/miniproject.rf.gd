<!DOCTYPE html>
<html>
<head>
  <title>Personalites</title>
  
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="Mohamed Slimani">
  <link href="<?php echo ROOT_PATH; ?>assets/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
  <link href="<?php echo ROOT_PATH; ?>assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <script src="<?php echo ROOT_PATH; ?>assets/vendor/jquery/jquery.js"></script>
  <script src="<?php echo ROOT_PATH; ?>assets/vendor/bootstrap/js/bootstrap.js"></script>
  <link  href="<?php echo ROOT_PATH; ?>assets/css/cropper.css" rel="stylesheet">
  <script src="<?php echo ROOT_PATH; ?>assets/js/cropper.js"></script>

  <link  href="<?php echo ROOT_PATH; ?>assets/css/animate.css" rel="stylesheet">
  <script src="<?php echo ROOT_PATH; ?>assets/vendor/bootstrap-notify/bootstrap-notify.min.js"></script>


  <link href="<?php echo ROOT_PATH; ?>assets/css/style.css" rel="stylesheet">
  
</head>

<body>
<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <?php if(!isset($_SESSION['is_logged_in'])) : ?>
        <a class="navbar-brand" href="<?php echo ROOT_PATH; ?>">HOME</a>
      <?php else : ?>
        <a class="navbar-brand" href="<?php echo ROOT_PATH; ?>Personalites">Personalités</a>
      <?php endif; ?>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
        <form class="navbar-form navbar-left" method="post" action="<?php echo ROOT_PATH; ?>Personalites/find">
          <?php if(isset($_SESSION['is_logged_in'])) : ?>
            <div class="input-group">
                <input type="text" class="form-control" name="name" placeholder="Search">
                <div class="input-group-btn">
                  <button class="btn btn-default" type="submit">
                    <i class="glyphicon glyphicon-search"></i>
                  </button>
                </div>
              </div>
          <?php endif; ?>
        </form>
            <ul class="nav navbar-nav navbar-right">
                <?php if(!isset($_SESSION['is_logged_in'])) : ?>
                  <li><a href="<?php echo ROOT_PATH; ?>users/login">Login <span class="fa fa-sign-in"></span></a></li>
                  <li><a href="<?php echo ROOT_PATH; ?>users/register">inscrit <span class="fa fa-user-plus"></span></a></li>
                <?php else : ?>
                  <li><a href="<?php echo ROOT_PATH; ?>users/profile">Welcome <?php echo $_SESSION['user_data']['first_name']; ?></a></li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        </li>
                        <li><a href="<?php echo ROOT_PATH; ?>users/settings"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="<?php echo ROOT_PATH; ?>users/logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <?php endif; ?>
            <!-- /.navbar-top-links -->


      </ul>
    </div>
  </div>
</nav>
<?php if(isset($_SESSION['is_logged_in'])) : ?>
    <!-- MetisMenu CSS -->
    <link href="<?php echo ROOT_PATH; ?>assets/vendor/metisMenu/metisMenu.css" rel="stylesheet">
    <script src="<?php echo ROOT_PATH; ?>assets/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom CSS -->
    <link href="<?php echo ROOT_PATH; ?>assets/dist/css/sb-admin-2.css" rel="stylesheet">
    <script src="<?php echo ROOT_PATH; ?>assets/dist/js/sb-admin-2.js"></script>
    
    <div style="height: 50px;"></div>
    <div id="wrapper">
    <?php require 'sidebar.php'; ?>
        <div id="page-wrapper">
        <div class="row">
    
        <?php require($view); ?>
        </div>
        </div>

    </div>
<?php else : ?>
    <?php require($view); ?>  
<?php endif; ?>      
<?php if(isset($_SESSION['Msg'])): ?>
<script type="text/javascript">
    $.notify({
        icon: '<?php Messages::geticon(); ?>',
        message: "<?php Messages::display(); ?>"
    },{
        type: '<?php Messages::getType(); ?>',
        offset: {
            x: 50,
            y: 60
        }
    });
</script>
<?php 
  unset($_SESSION['Msg']);
  endif;
 ?>
<script>
  function search() {

        var name =  document.getElementById('searchinput').value;
        console.log(name);
        document.getElementById('find').href = "<?php echo ROOT_PATH; ?>Personalites/find/"+name;
    }
  </script>
</body>
</html>