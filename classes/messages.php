<?php
class Messages{
	public static $type;

	public static function setMsg($text, $type){
		$_SESSION['Msg'] = array('type' => '', 'text'=> '' );
		$_SESSION['Msg']['type'] = $type;
		$_SESSION['Msg']['text'] = $text;
	}

	public static function geticon()
	{
		if($_SESSION['Msg']['type'] == 'error'){
			echo 'fa  fa-exclamation-triangle';
		} else {
			echo 'fa fa-check';
		}
	}

	public static function getType(){
		if($_SESSION['Msg']['type'] == 'error'){
			echo 'danger';
			} else {
			 echo 'success';
		}
	}


	public static function display(){
			echo $_SESSION['Msg']['text'];
	}
}