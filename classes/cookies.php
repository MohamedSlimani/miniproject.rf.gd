<?php
/**
* manage cookies
*/
class cookies
{
    public static function getCookies()
    {
        if(isset($_COOKIE["SM"])){
            if(!isset($_SESSION['is_logged_out'])){
                    $user_data = explode('|',$_COOKIE["SM"]);
                    $_SESSION['user_data']['id']            = $user_data[1];
                    $_SESSION['user_data']['username']      = $user_data[2];
                    $_SESSION['user_data']['first_name']    = $user_data[3];
                    $_SESSION['user_data']['last_name']     = $user_data[4];
                    $_SESSION['user_data']['email']         = $user_data[5];
                    $_SESSION['user_data']['img']           = $user_data[6];
                    $_SESSION['user_data']['sex']           = $user_data[7];
                    $_SESSION['user_data']['birthday']      = $user_data[8];
                    $_SESSION['user_data']['is_admin']      = $user_data[9];
                    $_SESSION['is_logged_in']               = 1 ;
            }else{
                session_destroy();
            }
        }
    }

    public static function putCookies()
    {

        $cookies = "";
        $expire = time() - 3600;

        if(isset($_SESSION['is_logged_in'])) {
            foreach ($_SESSION['user_data'] as $key => $value) {
                $cookies = $cookies .'|'.$value;    
            }
             $expire = time() + (86400 * 30);
        }
        setcookie("SM",$cookies, $expire, "/");
    }
}

?>