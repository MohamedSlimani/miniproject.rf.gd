<?php
abstract class Controller{
	protected $request;
	protected $action;
	protected $argument;

	public function __construct($action, $request, $argument){
		$this->action = $action;
		$this->request = $request;
		$this->argument = $argument;
	}

	public function executeAction(){
		return $this->{$this->action}($this->argument);
	}

	protected function returnView($viewmodel, $fullview){
		$view = 'views/'. get_class($this). '/' . $this->action. '.php';
		if($fullview){
			require('views/main.php');
		} else {
			require($view);
		}
	}
}
