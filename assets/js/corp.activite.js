function loadFile(event) {
    var output = document.getElementById('image');
    output.src = URL.createObjectURL(event.target.files[0]);
    $('#modal').modal();
}

function openFileOption(){
  document.getElementById("file1").click();
}

$(function () {
    var $image = $('#image');
    var cropBoxData;
    var canvasData;

    $('#modal').on('shown.bs.modal', function () {
        $image.cropper({
            aspectRatio: 1.5,
            autoCropArea: 1,
            ready: function () {
                $image.cropper('setCanvasData', canvasData);
                $image.cropper('setCropBoxData', cropBoxData);
            }  
        });
    }).on('hidden.bs.modal', function () {
        cropBoxData = $image.cropper('getCropBoxData');
        canvasData = $image.cropper('getCanvasData');
        document.getElementById('result').src = $image.cropper('getCroppedCanvas').toDataURL();
        document.getElementById('ProfilePicture').value = $image.cropper('getCroppedCanvas').toDataURL();
        $image.cropper('destroy');
    });

});
