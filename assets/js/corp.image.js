function confpass(){
    var pwd = document.getElementById('pwd');
    var pwdcmf = document.getElementById('pwdcmf');
    if(pwdcmf.value!=pwd.value){
        pwdcmf.setCustomValidity("Invalid field.");
    }else{
        pwdcmf.setCustomValidity("");
    }
}

function pass() {
    var pwd =  document.getElementById('pwd');
    if (pwd.value.length<8) {
        pwd.setCustomValidity("SVP entre un mot de pass plus grand que 8 caractères");
    }else{
        pwd.setCustomValidity("");
    }
}

function loadFile(event) {
    var output = document.getElementById('image');
    output.src = URL.createObjectURL(event.target.files[0]);
    $('#modal').modal();
}

function flag() {
    var optionindex = document.getElementById('Nationalite').selectedIndex;
    var options = document.getElementById('Nationalite').options;
    console.log('/uploads/Countries/'+options[optionindex].innerHTML+'.svg');
    document.getElementById('flag').src = '/uploads/Countries/'+options[optionindex].innerHTML+'.svg';
}


function openFileOption(){
  document.getElementById("file1").click();
}

$(function () {
    var $image = $('#image');
    var cropBoxData;
    var canvasData;

    $('#modal').on('shown.bs.modal', function () {
        $image.cropper({
            aspectRatio: 1,
            autoCropArea: 1,
            ready: function () {
                $image.cropper('setCanvasData', canvasData);
                $image.cropper('setCropBoxData', cropBoxData);
            }  
        });
    }).on('hidden.bs.modal', function () {
        cropBoxData = $image.cropper('getCropBoxData');
        canvasData = $image.cropper('getCanvasData');
        document.getElementById('result').src = $image.cropper('getCroppedCanvas').toDataURL();
        document.getElementById('ProfilePicture').value = $image.cropper('getCroppedCanvas').toDataURL();
        $image.cropper('destroy');
    });

});

function formsubmit() {
    //var data = CKEDITOR.instances.editor.getData();
    //document.getElementById('historique').value = data;
    document.getElementById('registration').submit();
}