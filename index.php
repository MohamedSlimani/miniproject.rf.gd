<?php
// Start Session
session_start();

// Include Config
require('config.php');



require('classes/Bootstrap.php');
require('classes/Controller.php');
require('classes/Model.php');
require('classes/cookies.php');
require('classes/messages.php');

if (!isset($_SESSION['is_logged_in']) && !isset($_SESSION['is_logged_out'])) {
	Cookies::getCookies();
}

require('controllers/home.php');
require('controllers/personalites.php');
require('controllers/users.php');
require('controllers/admin.php');
require('controllers/countries.php');
require('controllers/activitys.php');
require('controllers/statistics.php');


require('models/home.php');
require('models/personalite.php');
require('models/user.php');
require('models/admin.php');
require('models/countrie.php');
require('models/activity.php');
require('models/statistic.php');


$bootstrap = new Bootstrap($_GET);
$controller = $bootstrap->createController();
if($controller){
	$controller->executeAction();
}