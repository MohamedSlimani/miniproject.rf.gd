<?php

// Define DB Params
define("DB_HOST", "localhost");
define("DB_USER", "root");
define("DB_PASS", "");
define("DB_NAME", "mini-project");

// Define URL
define("ROOT_PATH", "/miniproject/");
define("ROOT_URL", "http://localhost/miniproject/");