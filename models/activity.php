<?php
class ActivityModel extends Model{
	//convert base64 string image to file
	private function base64_to_jpeg($base64_string, $output_file) {
	    $ifp = fopen($output_file, "wb"); 
	    $data = explode(',', $base64_string);
	    fwrite($ifp, base64_decode($data[1])); 
	    fclose($ifp);
	    return $output_file; 
	}

	public function Add(){
		// Sanitize POST
		$post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);	

		//the image variables
		$img = ($post['ProfilePicture']);
		$date = date('mdYHis', time());
		$location = 'uploads/Activitys/';
		$imageURL = $location.$post['avtivity'].'.png';
	

		if($post['submit']){

			//convert the image
			$this->base64_to_jpeg($img,$imageURL);
			
			// Insert into MySQL
			$this->query('INSERT INTO avtivite (label,img, description) VALUES(:label ,:img , :descreption)');
			$this->bind(':label', $post['avtivity']);
			$this->bind(':descreption', $post['descreption']);
			if ($post['ProfilePicture']==null) {
				$this->bind(':img', 'uploads/images/flag.svg');
			}else{
				$this->bind(':img', $imageURL);
			}
			$this->execute();
			$id = $this->lastInsertId();
			if($id){
				// Redirect
				//header('Location: '.ROOT_PATH.'users/login');

				Messages::setMsg('Avtivité ajoute avec success', '');
			}
			
		}
		
		return;
	}
}
