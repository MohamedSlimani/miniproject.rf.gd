<?php
class PersonalModel extends Model{

	private function base64_to_jpeg($base64_string, $output_file) {
	    $ifp = fopen($output_file, "wb"); 
	    $data = explode(',', $base64_string);
	    fwrite($ifp, base64_decode($data[1])); 
	    fclose($ifp);
	    return $output_file; 
	}

	public function id($name = '')
	{
		$this->query('SELECT * FROM personal WHERE N_ordre = :id');
		$this->bind(':id',$name);
		$result =  $this->single();
		
		$this->query('SELECT label FROM pays WHERE id = :id');
		$this->bind(':id',$result['pays_nais']);
		$temp = $this->single();
		$result['pays_nais'] = $temp['label'];
		
		$this->query('SELECT label FROM pays WHERE id = :id');
		$this->bind(':id',$result['pays_nat']);
		$temp = $this->single();
		$result['pays_nat'] = $temp['label'];

		$this->query('SELECT label FROM pays WHERE id = :id');
		$this->bind(':id',$result['paye']);
		$temp = $this->single();
		$result['paye'] = $temp['label'];
		
		
		return $result;
	}

	public function Index(){

		//basic statistics
		$this->query('SELECT COUNT(*) FROM personal');
		$temp =  $this->single();
		$result['nbrs']['personal'] =$temp['COUNT(*)'];
		$this->query('SELECT COUNT(*) FROM pays');
		$temp =  $this->single();
		$result['nbrs']['pays'] =$temp['COUNT(*)'];
		$this->query('SELECT COUNT(*) FROM avtivite');
		$temp =  $this->single();
		$result['nbrs']['activite'] =$temp['COUNT(*)'];
		
		//profile information
		$this->query('SELECT N_ordre,pays.label AS pays, first_name, last_name, email, image, avtivite.label AS activite FROM personal,pays,avtivite WHERE pays_nat = pays.id AND type_acitivite = avtivite.id  ORDER BY date_enrg DESC');
		$result['personal'] = $this->resultSet();

		return $result;
	}


	public function add(){
		$this->query('SELECT id,label FROM pays');
		$result['pays'] = $this->resultSet();

		$this->query('SELECT id,label FROM avtivite');
		$result['avtivite'] = $this->resultSet();

		$post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
				
		
		if(isset($post['submit'])){		
			$result['post'] = htmlspecialchars($_POST['historique']);	
			try {
			// Insert into MySQL
			$this->query('INSERT INTO personal (admin_id, pays_nat, adresse, ville, paye, email,	first_name,	last_name, dat_nais, lieu_nais, pays_nais, profession, address_prof, tel, debut_activie, type_acitivite, image, historique) VALUES(:id, :Nationalite, :Adresse, :Ville, :pays, :email, :first_name, :last_name, :birthday, :lieu_de_nissance ,:pays_de_nissance ,:Profession ,:Adresse_Profession ,:Telephone ,:Annee_debut ,:avtivite, :img ,:historique)');


			$this->bind(':id', $_SESSION['user_data']['id']);
			$this->bind(':Nationalite', $post['Nationalite']);
			$this->bind(':Adresse', $post['Adresse']);
			$this->bind(':Ville', $post['Ville']);
			$this->bind(':pays', $post['pays']);
			$this->bind(':email', $post['email']);
			$this->bind(':first_name', $post['first_name']);
			$this->bind(':last_name', $post['last_name']);
			$this->bind(':birthday', $post['birthday']);
			$this->bind(':lieu_de_nissance', $post['lieu_de_nissance']);
			$this->bind(':pays_de_nissance', $post['pays_de_nissance']);
			$this->bind(':Profession', $post['Profession']);
			$this->bind(':Adresse_Profession', $post['Adresse_Profession']);
			$this->bind(':Telephone', $post['Telephone']);
			$this->bind(':Annee_debut', $post['Annee_debut']);
			$this->bind(':avtivite', $post['avtivite']);
			$this->bind(':historique', htmlspecialchars($_POST['historique']));

			
			if ($post['ProfilePicture']==null) {
				$this->bind(':img', 'uploads/images/ProfilePicture.png');
			}else{

				//the image variables
				$img = ($post['ProfilePicture']);
				$date = date('mdYHis', time());
				$location = 'uploads/Profiles/';
				$imageURL = $location.$date.'.png';
				
				//convert the image
				$this->base64_to_jpeg($img,$imageURL);    

				$this->bind(':img', $imageURL);
			}
			
				$this->execute();
				Messages::setMsg('Personalité ajoute', '');


			} catch (Exception $e) {
				Messages::setMsg('somthing wrong', '');
				$result['err'] = $e;
			}
			
			$id = $this->lastInsertId();
			if($id){
				// Redirect
				//header('Location: '.ROOT_PATH.'users/login');
				Messages::setMsg('Personalité ajoute avec success', '');
			}
			
		}
		
		return $result;
	}

	public function find($name){

		$post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
		
		if(isset($post['name'])){
			$name = $post['name'];
		}
		//profile information
		$this->query("SELECT N_ordre,pays.label AS pays, first_name, last_name, email, image, avtivite.label AS activite FROM personal,pays,avtivite WHERE pays_nat = pays.id AND type_acitivite = avtivite.id AND (CONCAT(first_name, ' ', last_name) LIKE :name OR CONCAT(last_name, ' ', first_name) LIKE :name)");		
		$this->bind(':name', "$name%");
		$result['personal'] = $this->resultSet();
		$result['name'] = $name;

		return $result;
	}

	public function countries($name){
		if (!$name) {
			//profile information
			$this->query("SELECT id,label FROM pays ORDER BY label");		
			$result['pays'] = $this->resultSet();
			foreach ($result['pays'] as $key => $pays) {
				$this->query('SELECT COUNT(*) FROM personal WHERE pays_nat = :id ');
				$this->bind(':id', $pays['id']);
				$temp =  $this->single();
				$result['nbrs'][$pays['id']] =$temp['COUNT(*)'];
			
			}
		}else{
			$this->query("SELECT N_ordre,pays.label AS pays, first_name, last_name, email, image, avtivite.label AS activite FROM personal,pays,avtivite WHERE pays_nat = pays.id AND type_acitivite = avtivite.id AND pays.id = :name");
				$this->bind(':name', $name);
			$result['personal'] = $this->resultSet();
		}
		return $result;
	}

	public function avtivites($name){

		if (!$name) {
			//profile information
			$this->query("SELECT id,label,img FROM avtivite ORDER BY label");		
			$result['avtivite'] = $this->resultSet();
			foreach ($result['avtivite'] as $key => $avtivite) {
				$this->query('SELECT COUNT(*) FROM personal WHERE type_acitivite = :id ');
				$this->bind(':id', $avtivite['id']);
				$temp =  $this->single();
				$result['nbrs'][$avtivite['id']] =$temp['COUNT(*)'];
			
			}
		}else{
			$this->query("SELECT N_ordre,pays.label AS pays, first_name, last_name, email, image, avtivite.label AS activite FROM personal,pays,avtivite WHERE pays_nat = pays.id AND type_acitivite = avtivite.id AND avtivite.id = :name");
				$this->bind(':name', $name);
			$result['personal'] = $this->resultSet();
		}
		
		return $result;
	}

	public function modify($id){
		$this->query('SELECT id,label FROM pays');
		$result['pays'] = $this->resultSet();

		$this->query('SELECT id,label FROM avtivite');
		$result['avtivite'] = $this->resultSet();

		$this->query('SELECT * FROM personal WHERE N_ordre = :id');
		$this->bind(':id', $id);
		$result['personal'] = $this->single();

		$post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
				
		
		if(isset($post['submit'])){		
			$result['post'] = htmlspecialchars($_POST['historique']);	
			try {
			// Insert into MySQL
			$this->query('UPDATE personal SET pays_nat = :Nationalite, adresse = :Adresse, ville = :Ville, paye = :pays, email = :email, first_name = :first_name, last_name = :last_name, dat_nais = :birthday, lieu_nais = :lieu_de_nissance, pays_nais = :pays_de_nissance, profession = :Profession, address_prof = :Adresse_Profession, tel = :Telephone, debut_activie = :Annee_debut, type_acitivite = :avtivite, historique = :historique WHERE N_ordre = :id');


			$this->bind(':Nationalite', $post['Nationalite']);
			$this->bind(':Adresse', $post['Adresse']);
			$this->bind(':Ville', $post['Ville']);
			$this->bind(':pays', $post['pays']);
			$this->bind(':email', $post['email']);
			$this->bind(':first_name', $post['first_name']);
			$this->bind(':last_name', $post['last_name']);
			$this->bind(':birthday', $post['birthday']);
			$this->bind(':lieu_de_nissance', $post['lieu_de_nissance']);
			$this->bind(':pays_de_nissance', $post['pays_de_nissance']);
			$this->bind(':Profession', $post['Profession']);
			$this->bind(':Adresse_Profession', $post['Adresse_Profession']);
			$this->bind(':Telephone', $post['Telephone']);
			$this->bind(':Annee_debut', $post['Annee_debut']);
			$this->bind(':avtivite', $post['avtivite']);
			$this->bind(':historique', htmlspecialchars($_POST['historique']));
			$this->bind(':id', $id);

			$this->execute();


			if ($post['ProfilePicture']!=null) {
				//the image variables
				$img = ($post['ProfilePicture']);
				$date = date('mdYHis', time());
				$location = 'uploads/Profiles/';
				$imageURL = $location.$date.'.png';
				
				//convert the image
				$this->base64_to_jpeg($img,$imageURL);

				$this->query('UPDATE personal SET image = :img WHERE N_ordre = :id');
				$this->bind(':id', $id);
				$this->bind(':img', $imageURL);
				$this->execute();
				Messages::setMsg(' les information de Personalité ont bien met a jour', '');
			}				
			
			


			} catch (Exception $e) {
				Messages::setMsg('somthing wrong', '');
				$result['err'] = $e;
			}
			
		}
		
		$this->query('SELECT * FROM personal WHERE N_ordre = :id');
		$this->bind(':id', $id);
		$result['personal'] = $this->single();

		return $result;
	}

	public function delete($id){
		$post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
		
		if(isset($post['submit'])){		
			try {

				// Delete from MySQL
				$this->query('DELETE FROM personal WHERE N_ordre = :id');
				$this->bind(':id',$id);
				$this->execute();
				Messages::setMsg('Personalité suppremier', '');
				header('Location: '.ROOT_PATH);

			} catch (Exception $e) {
				Messages::setMsg('somthing wrong : '.$e, '');
			}
			
		}else{

		$this->query('SELECT N_ordre, first_name, last_name, email, image, pays_nat, type_acitivite FROM personal WHERE N_ordre = :id');
		$this->bind(':id',$id);
		$result['personal'] = $this->single();
		
		$this->query('SELECT label FROM pays WHERE id = :id');
		$this->bind(':id',$result['personal']['pays_nat']);
		$result['pays'] = $this->single();

		$this->query('SELECT label FROM avtivite WHERE id = :id');
		$this->bind(':id',$result['personal']['type_acitivite']);
		$result['activite'] = $this->single();

		}

		return $result;
	}

}