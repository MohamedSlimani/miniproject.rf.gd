<?php
class UserModel extends Model{
	//convert base64 string image to file
	private function base64_to_jpeg($base64_string, $output_file) {
	    $ifp = fopen($output_file, "wb"); 
	    $data = explode(',', $base64_string);
	    fwrite($ifp, base64_decode($data[1])); 
	    fclose($ifp);
	    return $output_file; 
	}

	public function register(){
		// Sanitize POST
		$post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

		$password = md5($post['password']);
		
		//the image variables
		$img = ($post['ProfilePicture']);
		$date = date('mdYHis', time());
		$location = 'uploads/Profiles/';
		$imageURL = $location.$date.'.png';

		if($post['submit']){
			
			//convert the image
			$this->base64_to_jpeg($img,$imageURL);    

			// Insert into MySQL
			$this->query('INSERT INTO user (first_name, last_name, username, email, password, sex, birthday, img) VALUES(:first_name, :last_name, :username, :email, :password, :sex, :birthday, :img)');
			$this->bind(':first_name', $post['first_name']);
			$this->bind(':last_name', $post['last_name']);
			$this->bind(':username', $post['first_name'].$date);
			$this->bind(':birthday', $post['birthday']);
			$this->bind(':sex', $post['sex']);
			$this->bind(':email', $post['email']);
			$this->bind(':password', $password);
			if ($post['ProfilePicture']==null) {
				$this->bind(':img', 'uploads/images/ProfilePicture.png');
			}else{
				$this->bind(':img', $imageURL);
			}
			
			$this->execute();
			$id = $this->lastInsertId();
			if($id){
				// Redirect
				header('Location: '.ROOT_PATH.'users/login');
			}
			
		}
		return;
	}

	public function login(){
		// Sanitize POST
		$post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

		$password = md5($post['password']);

		if($post['submit']){
			// Compare Login
			$this->query('SELECT * FROM user WHERE email = :email AND password = :password');
			$this->bind(':email', $post['email']);
			$this->bind(':password', $password);
			
			$row = $this->single();

			if($row){
				$_SESSION['is_logged_in'] = true;
				$_SESSION['user_data'] = array(
					"id"			=> $row['id'],
					"username"		=> $row['username'],
					"first_name"	=> $row['first_name'],
					"last_name"		=> $row['last_name'],
					"email"			=> $row['email'],
					"img"			=> $row['img'],
					"sex"			=> $row['sex'],
					"birthday"		=> $row['birthday'],
					"is_admin"		=> $row['is_admin']
				);

				Cookies::putCookies();	
				
				if ($_SESSION['user_data']['is_admin']) {
					header('Location: '.ROOT_PATH.'Personalites');
				}else{
					header('Location: '.ROOT_PATH.'Personalites');
				}
				
			} else {
				Messages::setMsg('Incorrect Login', 'error');
			}
		}
		return;
	}

	public function profile($username){
		//profile information
		$result = array(
			'user_data' => '',
			'posts'=>'',
			'modifie' =>''
			);

		//if no username get the session username
		if($username=='') {
			$username = $_SESSION['user_data']['username'];
		}

		//if the user name == the session user name enable modification
		if ($username == $_SESSION['user_data']['username']) {
			$result['modifie'] = true;
		}

		$this->query('SELECT * FROM user WHERE username = :username');
		$this->bind(':username', $username);
		$row = $this->single();
		$result['user_data'] = $row;
		if($result['user_data']['is_admin']){
			$this->query('SELECT * FROM personal WHERE admin_id = :id ORDER BY create_date DESC');
			$this->bind(':id', $_SESSION['user_data']['id']);
			$rows = $this->resultSet();
			$result['posts'] = $rows;	
		}
		
		if ($row) {
			return $result;
		}else{
			Messages::setMsg('Incorrect User URL', 'error');
		}
		return;
	}
	public function settings(){
		// Sanitize POST
		$post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
		
		if($post['submit']){
			
			if (isset($post['changePass'])) {
				//the second form to change password

				$this->query('SELECT password from user WHERE id = :id');
				$this->bind(':id',$_SESSION['user_data']['id']);
				
				if(md5($post['password']) == $this->single()['password']){ //verivay the passowrd
					$this->query('UPDATE user SET password = :password WHERE id = :id');
					$this->bind(':password',md5($post['newpassword']));
					$this->bind(':id',$_SESSION['user_data']['id']);
					$this->execute();

					Messages::setMsg('votr mot de pass a ete changer', '');
				}else{
					Messages::setMsg('mot de pass incorrect', 'error');
				}


			}else{

				//the first form to change the other data

				
				// check if a user name exist
				$this->query('SELECT id FROM user WHERE username = :username');
				$this->bind(':username', $post['username']);
				$row = $this->single();
				if (isset($row['id'])) {
					if($row['id'] != $_SESSION['user_data']['id']){
						$id = 1;
					}
				}


				if (!isset($id)) {
					// Insert into MySQL
					$this->query('UPDATE user SET first_name = :first_name, last_name = :last_name, username = :username, birthday =:birthday, email =:email, sex =:sex WHERE id = :id');
					$this->bind(':first_name', $post['first_name']);
					$this->bind(':last_name', $post['last_name']);
					$this->bind(':username', $post['username']);
					$this->bind(':birthday', $post['birthday']);
					$this->bind(':email', $post['email']);
					$this->bind(':sex', $post['sex']);
					$this->bind(':id', $_SESSION['user_data']['id']);
					$this->execute();
					
					$_SESSION['user_data']['first_name'] = $post['first_name'];
					$_SESSION['user_data']['last_name'] = $post['last_name'];
					$_SESSION['user_data']['username'] = $post['username'];
					$_SESSION['user_data']['birthday'] = $post['birthday'];
					$_SESSION['user_data']['email'] = $post['email'];
					$_SESSION['user_data']['sex'] = $post['sex'];
					

					//the image variables
					if ($post['ProfilePicture']!=null) {
						$img = ($post['ProfilePicture']);
						if ($img!=$_SESSION['user_data']['img']) {
							$date = date('mdYHis', time());
							$location = 'uploads/Profiles/';
							$imageURL = $location.$date.'.png';
							
							//convert the image
							$this->base64_to_jpeg($img,$imageURL);
							$this->query('UPDATE user SET img = :img WHERE id = :id');
							$this->bind(':img', $imageURL);
							$this->bind(':id', $_SESSION['user_data']['id']);
							$this->execute();
							$_SESSION['user_data']['img'] = $imageURL;
								
						}
					}

					$_SESSION['update'] = 1;

					Cookies::putCookies();
					Messages::setMsg('vos information ont bien mettre a jour', '');
				}else{
					Messages::setMsg("le nom d'utilisateur exist deja", 'error');
				}
			
			}
		}
		return;
	}
}