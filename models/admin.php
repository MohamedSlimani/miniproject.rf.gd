<?php
class AdminModel extends Model{
	

	//convert base64 string image to file
	private function base64_to_jpeg($base64_string, $output_file) {
	    $ifp = fopen($output_file, "wb"); 
	    $data = explode(',', $base64_string);
	    fwrite($ifp, base64_decode($data[1]));
	    fclose($ifp);
	    return $output_file; 
	}

	public function AddUser(){
		// Sanitize POST
		$post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

		$password = md5($post['password']);
		
		//the image variables
		$img = ($post['ProfilePicture']);
		$date = date('mdYHis', time());
		$location = 'uploads/Profiles/';
		$imageURL = $location.$date.'.png';

		if($post['submit']){
			
			//convert the image
			if($img)
			$this->base64_to_jpeg($img,$imageURL);    

			// Insert into MySQL
			$this->query('INSERT INTO user (first_name, last_name, username, email, password, sex, birthday, img, add_by) VALUES(:first_name, :last_name, :username, :email, :password, :sex, :birthday, :img, :add_by)');
			$this->bind(':first_name', $post['first_name']);
			$this->bind(':last_name', $post['last_name']);
			$this->bind(':username', $post['first_name'].$date);
			$this->bind(':birthday', $post['birthday']);
			$this->bind(':sex', $post['sex']);
			$this->bind(':email', $post['email']);
			$this->bind(':add_by', $_SESSION['user_data']['id']);
			$this->bind(':password', $password);
			$this->bind(':img', $imageURL);
			$this->execute();
			$id = $this->lastInsertId();
			if($id){
				Messages::setMsg('utilistateur ajouté avec success', '');
				// Redirect
				//header('Location: '.ROOT_URL.'users/login');
			}
			
		}
		
		return;
	}

	public function ModifyUser($username)
	{
		//profile information
		$result = 0;
		if ($username == '') {
			// Redirect
			header('Location: '.ROOT_URL.'admin/choseuser/');
		}else{
			$this->query('SELECT * FROM user WHERE username = :username');
			$this->bind(':username', $username);
			$result = $this->single();
			Messages::setMsg('utilistateur modifé avec success', '');

		}
		return $result;
	}

	public function ChoseUser()
	{
		//profile information
		$this->query('SELECT first_name, last_name, username, email, img FROM user WHERE is_admin <> 1 ORDER BY username');
		$result = $this->resultSet();

		
		return $result;
	}

	public function DeletUser($username)
	{	
		$result = 0;
		if ($username == '') {
			// Redirect
			header('Location: '.ROOT_URL.'admin/choseuser/');
		}
		$post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

		$this->query('SELECT * FROM user WHERE username = :username');
		$this->bind(':username', $username);
		$result = $this->single();

		if($post['submit']){
			$this->query('DELETE FROM user WHERE username = :username');
			$this->bind(':username', $username);
			$this->execute();
			header('Location: '.ROOT_URL.'admin/choseuser');
			Messages::setMsg('utilistateur ajouté avec success', '');	
		}

		return $result;	
	}
}
