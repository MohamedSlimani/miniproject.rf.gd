<?php
class CountrieModel extends Model{
	

	public function Add(){
		// Sanitize POST
		$post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);		
		if($post['submit']){
			
			//the image variables
			$location 	= "uploads/Countries/";
			$name       = $post['paye'].'.svg';
			$temp_name  = $_FILES['ProfilePicture']['tmp_name'];
			
			if(move_uploaded_file($temp_name, $location.$name)){
                $imageURL = $location.$name;
            }


			// Insert into MySQL
			$this->query('INSERT INTO pays (label, descreption, img) VALUES(:label, :descreption, :img)');
			$this->bind(':label', $post['paye']);
			$this->bind(':descreption', $post['descreption']);
			$this->bind(':img', $imageURL);
			$this->execute();
			$id = $this->lastInsertId();
			if($id){
				// Redirect
				//header('Location: '.ROOT_PATH.'users/login');

				Messages::setMsg('Paye ajoute avec success', '');
			}
			
		}
		
		return;
	}
}
