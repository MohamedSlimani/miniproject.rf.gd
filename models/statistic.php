<?php
class StatisticModel extends Model{
	public function index()
	{	
		//get the natinalites table
		//1 get the nat id
		$this->query('SELECT DISTINCT pays_nat FROM personal');
		$row = $this->resultSet();
		//2 get the lable foreach id
		foreach ($row as $key => $data) {
			$result['pays_nat'][]=$data['pays_nat'];
		}
		$row = NULL;
		//2 get the lable foreach id
		foreach ($result['pays_nat'] as $key => $pays_nat) {
			$this->query('SELECT label FROM pays WHERE id = :id');
			$this->bind(':id',$pays_nat);
			$row[] = $this->single();
		}
		foreach ($row as $key => $data) {
			$result['nat'][]=$data['label'];
		}
		//merg theme id => lable
		$result['nat'] = array_combine($result['pays_nat'], $result['nat']);
		unset($result['pays_nat']);

		//get the Activites table
		//1 get the nat id
		$this->query('SELECT DISTINCT type_acitivite FROM personal');
		$row = $this->resultSet();
		//2 get the lable foreach id
		foreach ($row as $key => $data) {
			$result['type_acitivite'][]=$data['type_acitivite'];
		}
		$row = NULL;
		//2 get the lable foreach id
		foreach ($result['type_acitivite'] as $key => $type_acitivite) {
			$this->query('SELECT label FROM avtivite WHERE id = :id');
			$this->bind(':id',$type_acitivite);
			$row[] = $this->single();
		}
		foreach ($row as $key => $data) {
			$result['Activites'][]=$data['label'];
		}
		//merg theme id => lable
		$result['Activites'] = array_combine($result['type_acitivite'], $result['Activites']);
		unset($result['type_acitivite']);


		return $result;
	}

	public function activiteyear()
	{
		// Sanitize POST
		$post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);


		$this->query('SELECT pays.label AS pays,COUNT(pays.label) as nbr FROM personal,pays WHERE pays_nat = pays.id GROUP BY pays.label'); 
		//add this condition when you make a lot of personlites;
		//AND personal.debut_activie = :start
		$this->bind(':start', $post['date_debut']);
		$result['pays'] = $this->resultSet();
		$result['date_debut'] = $post['date_debut'];
		return $result;
	}

	public function natactivite()
	{	
		$post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
		$this->query('SELECT label AS act,COUNT(label) as nbr FROM personal,avtivite WHERE type_acitivite = avtivite.id GROUP BY label');
		$this->bind(':nat', $post['nat']);
		$result['activite'] = $this->resultSet();

		$this->query('SELECT label FROM pays WHERE id =  :nat');
		$this->bind(':nat', $post['nat']);
		$result['nat'] = $this->single();
		

		return $result;
	
	}

	public function natdate()
	{	
		$post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
		$this->query('SELECT debut_activie ,COUNT(personal.debut_activie) as nbr FROM personal WHERE 1 GROUP BY personal.debut_activie');
		//personal.pays_nat = :nat
		$this->bind(':nat', $post['nat']);
		$result['dates'] = $this->resultSet();

		$this->query('SELECT label FROM pays WHERE id =  :nat');
		$this->bind(':nat', $post['nat']);
		$result['nat'] = $this->single();
		
		return $result;
	
	}

}

?>